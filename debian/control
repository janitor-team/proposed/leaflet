Source: leaflet
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders:
 Andrew Harvey <andrew.harvey4@gmail.com>,
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 brotli,
 debhelper-compat (= 13),
 eslint <!nocheck>,
 node-js-yaml,
 node-rollup-plugin-json (>= 4.1),
 pigz,
 rename,
 rollup,
 sassc,
 uglifyjs,
 terser,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/js-team/leaflet
Vcs-Git: https://salsa.debian.org/js-team/leaflet.git
Homepage: https://leafletjs.com/
Rules-Requires-Root: no

Package: node-leaflet
Architecture: all
Depends:
 libjs-leaflet,
 nodejs:any,
 ${misc:Depends},
Description: mobile-friendly interactive maps - Node.js library
 Leaflet is a modern JavaScript library for mobile-friendly interactive
 maps.  Weighing just about 27 KB of JS code, it has all the features
 most developers ever need for online maps.
 .
 Leaflet is designed with simplicity, performance and usability in mind.
 It works efficiently across all major desktop and mobile platforms out
 of the box, taking advantage of HTML5 and CSS3 on modern browsers while
 still being accessible on older ones. It can be extended with many
 plugins, has a beautiful, easy to use and well-documented API and a
 simple, readable source code that is a joy to contribute to.
 .
 This package provides Leaflet library usable with Node.js -
 an event-based server-side JavaScript engine.

Package: libjs-leaflet
Architecture: all
Depends:
 ${misc:Depends},
Recommends:
 javascript-common,
Multi-Arch: foreign
Description: mobile-friendly interactive maps - browser library
 Leaflet is a modern JavaScript library for mobile-friendly interactive
 maps.  Weighing just about 27 KB of JS code, it has all the features
 most developers ever need for online maps.
 .
 Leaflet is designed with simplicity, performance and usability in mind.
 It works efficiently across all major desktop and mobile platforms out
 of the box, taking advantage of HTML5 and CSS3 on modern browsers while
 still being accessible on older ones. It can be extended with many
 plugins, has a beautiful, easy to use and well-documented API and a
 simple, readable source code that is a joy to contribute to.
 .
 This package provides Leaflet library directly usable in web browsers.
